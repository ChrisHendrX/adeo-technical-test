const data = require('../data').data;
const { test1, test2 } = require('../mocks');
const { expect } = require('chai');
const { filterAnimals, countChildren } = require('../app');

describe('#filterAnimals(data, string)', function() {
  it('should return the initial data if the filter value is empty', function() {
    const filtered = filterAnimals(data, '');
    expect(filtered).to.eql(data);
  });

  it('should return the initial data if the filter value is not a string', function() {
    const filtered = filterAnimals(data, null);
    expect(filtered).to.eql(data);
  });

  it('should return one country with one person owning an animal named "Deer Mouse"', function() {
    const filtered = filterAnimals(data, 'Deer Mouse');
    expect(filtered).to.eql(test1);
  });

  it('should return an empty array if the given animal name does not exist', function() {
    const filtered = filterAnimals(data, 'Unicorn');
    expect(filtered).to.eql([]);
  });

  it('should return an array of animals whose name contains the string "ry"', function() {
    const filtered = filterAnimals(data, 'ry');
    expect(filtered).to.eql(test2);
  });

  it('should throw an error if data is not an array', function() {
    // expect(filterAnimals(null)).to.throw(Error);
  });
});

describe('#countChildren(data)', function() {
  it('should return an array with the same length as the initial data', function() {
    const withCount = countChildren(data);
    expect(withCount.length).to.be.equal(data.length)
  });
  it ('should return every parent name with the count of its children into square brackets', function() {
    const withCount = countChildren(data);
    expect(withCount[0].name).to.be.equal(`${data[0].name} [${data[0].people.length}]`);
    expect(withCount[0].people[0].name).to.be.equal(`${data[0].people[0].name} [${data[0].people[0].animals.length}]`);
  })
});
