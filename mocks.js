const test1 = [
  {
    name: 'Dillauti',
    people: [
      {
        name: 'Blanche Viciani',
        animals: [{ name: 'Deer Mouse' }]
      }
    ]
  }
];

const test2 = [
  {
    name: 'Uzuzozne',
    people: [
      {
        name: 'Lillie Abbott',
        animals: [{ name: 'John Dory' }]
      }
    ]
  },
  {
    name: 'Satanwi',
    people: [
      {
        name: 'Anthony Bruno',
        animals: [{ name: 'Oryx' }]
      }
    ]
  }
];

module.exports = {
  test1,
  test2
};
